package com.oop.inheritance;

class Levelone 
{ 
    public void print_classOne() 
    { 
        System.out.println("Inside Class one"); 
    } 
} 
  
class Levaltwo extends Levelone 
{ 
    public void print_classTwo() 
    { 
        System.out.println("inside class two"); 
    } 
} 
  
class Levalthree extends Levaltwo 
{ 
    public void print_classThree() 
    { 
        System.out.println("Inside Class Three"); 
    } 
} 




public class Multilevel_Inheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Levalthree g = new Levalthree(); 
        g.print_classOne(); 
        g.print_classTwo(); 
        g.print_classThree();
	}

}
