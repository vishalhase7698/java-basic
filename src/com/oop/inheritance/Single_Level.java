package com.oop.inheritance;


class one 
{ 
    public void print_one() 
    { 
        System.out.println("class One"); 
    } 
} 
  
class two extends one 
{ 
    public void print_two() 
    { 
        System.out.println("class two"); 
    } 
}

public class Single_Level {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		two g = new two(); 
        g.print_one(); 
        g.print_two(); 
        
	}

}
