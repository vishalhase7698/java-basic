package com.oop.inheritance;

class Parent 
{ 
    public void print_Parent() 
    { 
        System.out.println("Parent"); 
    } 
} 
  
class Childone extends Parent 
{ 
    public void print_Child1() 
    { 
        System.out.println("print_Child1"); 
    } 
} 
  
class Childtwo extends Parent 
{ 
    public void print_Child2() 
    { 
        System.out.println("print_Child1"); 
    } 
} 

public class Hirachical {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Childone g = new Childone(); 
        g.print_Child1(); 
        Childtwo t = new Childtwo(); 
        t.print_Child2(); 
        g.print_Parent(); 

	}

}
